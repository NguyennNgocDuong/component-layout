import logo from './logo.svg';
import './App.css';
import Header from './components/Header.jsx'
import Banner from './components/Banner.jsx'
import ListItem from './components/ListItem.jsx'
import Footer from './components/Footer.jsx'

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <ListItem />
      <Footer />
    </div>
  );
}

export default App;
